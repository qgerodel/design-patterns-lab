package eu.telecomnancy.sensor.proxy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.sensor.SimpleSensorLogger;
import eu.telecomnancy.sensor.LogLevel;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import java.util.Date;

public class LoggedTemperatureSensor extends SimpleSensorLogger implements ISensor {

  TemperatureSensor temperatureSensor;

  public LoggedTemperatureSensor(){
    temperatureSensor = new TemperatureSensor();
  }

  public LoggedTemperatureSensor(TemperatureSensor existingSensor){
    temperatureSensor = existingSensor;
  }

  private String header() {
    Date now = new Date();

    return "[" + now + "] ";
  }

  @Override
  public void on() {
      temperatureSensor.on();
      this.log(LogLevel.INFO, this.header() + "TemperatureSensor.on() => " + "Null");
  }

  @Override
  public void off() {
      temperatureSensor.off();
      this.log(LogLevel.INFO, this.header() + "TemperatureSensor.off() => " + "Null");
  }

  @Override
  public boolean getStatus() {
      boolean value = temperatureSensor.getStatus();
      this.log(LogLevel.INFO, this.header() + "TemperatureSensor.on() => " + value);
      return value;
  }

  @Override
  public void update() throws SensorNotActivatedException {
    try {
      this.log(LogLevel.INFO, this.header() + "TemperatureSensor.update() => " + "Null");
      temperatureSensor.update();
    }
    catch (SensorNotActivatedException e) {
      throw e;
    }
  }

  @Override
  public double getValue() throws SensorNotActivatedException {
    try {
      double value = temperatureSensor.getValue();
      this.log(LogLevel.INFO, this.header() + "TemperatureSensor.getValue() => " + value);
      return value;
    }
    catch (SensorNotActivatedException e) {
      throw e;
    }
  }

}
