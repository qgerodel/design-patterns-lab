package eu.telecomnancy.sensor.decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class RoundedTemperatureSensor implements ISensor {

  TemperatureSensor temperatureSensor;

  public RoundedTemperatureSensor(TemperatureSensor existingSensor){
    temperatureSensor = existingSensor;
  }

  @Override
  public void on() {
      temperatureSensor.on();
  }

  @Override
  public void off() {
      temperatureSensor.off();
  }

  @Override
  public boolean getStatus() {
      return temperatureSensor.getStatus();
  }

  @Override
  public void update() throws SensorNotActivatedException {
    try {
      temperatureSensor.update();
    }
    catch (SensorNotActivatedException e) {
      throw e;
    }
  }

  @Override
  public double getValue() throws SensorNotActivatedException {
    try {
      return Math.round(temperatureSensor.getValue());
    }
    catch (SensorNotActivatedException e) {
      throw e;
    }
  }

}
