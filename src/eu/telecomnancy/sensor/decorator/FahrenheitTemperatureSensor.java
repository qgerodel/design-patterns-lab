package eu.telecomnancy.sensor.decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class FahrenheitTemperatureSensor implements ISensor {

  TemperatureSensor temperatureSensor;

  public FahrenheitTemperatureSensor(TemperatureSensor existingSensor){
    temperatureSensor = existingSensor;
  }

  @Override
  public void on() {
      temperatureSensor.on();
  }

  @Override
  public void off() {
      temperatureSensor.off();
  }

  @Override
  public boolean getStatus() {
      return temperatureSensor.getStatus();
  }

  @Override
  public void update() throws SensorNotActivatedException {
    try {
      temperatureSensor.update();
    }
    catch (SensorNotActivatedException e) {
      throw e;
    }
  }

  @Override
  public double getValue() throws SensorNotActivatedException {
    try {
      return temperatureSensor.getValue()*1.8+32;
    }
    catch (SensorNotActivatedException e) {
      throw e;
    }
  }

}
