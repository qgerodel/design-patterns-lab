package eu.telecomnancy.sensor;

import java.util.Random;
import java.util.Observable;

public class TemperatureSensor extends Observable implements ISensor {
    boolean state;
    double value = 0;

    private void sendNotif() {
      this.setChanged();
      this.notifyObservers();
    }

    @Override
    public void on() {
        state = true;
        this.sendNotif();
    }

    @Override
    public void off() {
        state = false;
        this.sendNotif();
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state) {
            value = (new Random()).nextDouble() * 100;
            this.sendNotif();
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
