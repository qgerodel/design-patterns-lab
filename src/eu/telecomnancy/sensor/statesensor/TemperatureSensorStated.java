package eu.telecomnancy.sensor.statesensor;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class TemperatureSensorStated implements ISensor {
    TemperatureSensorState temperatureState;

    public TemperatureSensorStated(){
      temperatureState = new TemperatureSensorOff();
    }

    @Override
    public void on() {
        temperatureState = new TemperatureSensorOn();
    }

    @Override
    public void off() {
        temperatureState = new TemperatureSensorOff();
    }

    @Override
    public boolean getStatus() {
        return temperatureState.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
      try {
        temperatureState.update();
      }
      catch (SensorNotActivatedException e) {
        throw e;
      }
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
      try {
        return temperatureState.getValue();
      }
      catch (SensorNotActivatedException e) {
        throw e;
      }
    }

}
