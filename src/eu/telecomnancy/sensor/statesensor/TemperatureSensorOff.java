package eu.telecomnancy.sensor.statesensor;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class TemperatureSensorOff implements TemperatureSensorState {
    @Override
    public void on() {
      // Do nothing
    }

    @Override
    public void off() {
      // Do nothing
    }

    @Override
    public boolean getStatus() {
        return false;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
