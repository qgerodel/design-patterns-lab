package eu.telecomnancy.sensor.statesensor;

import java.util.Random;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class TemperatureSensorOn implements TemperatureSensorState {
    double value = 0;

    @Override
    public void on() {
        // Do nothing
    }

    @Override
    public void off() {
        // Do nothing
    }

    @Override
    public boolean getStatus() {
        return true;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        value = (new Random()).nextDouble() * 100;
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return value;
    }

}
