package eu.telecomnancy.sensor;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;

import eu.telecomnancy.sensor.proxy.LoggedTemperatureSensor;
import eu.telecomnancy.sensor.adapter.LegacyTemperatureSensorAdapter;
import eu.telecomnancy.sensor.statesensor.TemperatureSensorStated;
import eu.telecomnancy.sensor.decorator.RoundedTemperatureSensor;
import eu.telecomnancy.sensor.decorator.FahrenheitTemperatureSensor;

public class SensorFactory {

  public ISensor getSensor(String sensorType) {
    if(sensorType == null){
       return null;
    }
    if(sensorType.equalsIgnoreCase("TemperatureSensor")){
       return new TemperatureSensor();
    } else if(sensorType.equalsIgnoreCase("LoggedTemperatureSensor")){
       return new LoggedTemperatureSensor();
    } else if(sensorType.equalsIgnoreCase("LegacyTemperatureSensor")){
       return new LegacyTemperatureSensorAdapter();
    } else if(sensorType.equalsIgnoreCase("TemperatureSensorStated")){
       return new TemperatureSensorStated();
    } else if(sensorType.equalsIgnoreCase("RoundedTemperatureSensor")){
       return new RoundedTemperatureSensor(new TemperatureSensor());
    } else if(sensorType.equalsIgnoreCase("FahrenheitTemperatureSensor")){
       return new FahrenheitTemperatureSensor(new TemperatureSensor());
    }

    return null;
  }

}
