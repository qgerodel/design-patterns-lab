package eu.telecomnancy.sensor.adapter;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class LegacyTemperatureSensorAdapter implements ISensor {
    LegacyTemperatureSensor legacySensor;

    public LegacyTemperatureSensorAdapter(){
      legacySensor = new LegacyTemperatureSensor();
    }

    public LegacyTemperatureSensorAdapter(LegacyTemperatureSensor existingSensor){
      legacySensor = existingSensor;
    }

    @Override
    public void on() {
      if (!this.getStatus()) {
        legacySensor.onOff();
      }
    }

    @Override
    public void off() {
      if (this.getStatus()) {
        legacySensor.onOff();
      }
    }

    @Override
    public boolean getStatus() {
        return legacySensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
      if(this.getStatus()) {
        this.off();
        this.on();
      }
      else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (this.getStatus())
            return legacySensor.getTemperature();
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
