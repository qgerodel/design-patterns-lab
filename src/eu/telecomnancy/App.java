package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.ui.ConsoleUI;

public class App {

    public static void main(String[] args) {
        SensorFactory factory = new SensorFactory();
        ISensor sensor = factory.getSensor("LoggedTemperatureSensor");
        new ConsoleUI(sensor);
    }

}
