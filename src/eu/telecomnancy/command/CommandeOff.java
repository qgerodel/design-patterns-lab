package eu.telecomnancy.command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;


public class CommandeOff implements CommandeInterface {
   private ISensor sensor;

   public CommandeOff(ISensor sensor){
      this.sensor = sensor;
   }

   public void execute() throws SensorNotActivatedException{
      sensor.off();
   }
}
