package eu.telecomnancy.command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeUpdate implements CommandeInterface {
   private ISensor sensor;

   public CommandeUpdate(ISensor sensor){
      this.sensor = sensor;
   }

   public void execute() throws SensorNotActivatedException{
      try {
        sensor.update();
      }
      catch (SensorNotActivatedException e) {
        throw e;
      }
   }
}
