package eu.telecomnancy.command;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface CommandeInterface {
   public void execute() throws SensorNotActivatedException;
}
