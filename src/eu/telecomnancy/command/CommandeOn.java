package eu.telecomnancy.command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeOn implements CommandeInterface {
   private ISensor sensor;

   public CommandeOn(ISensor sensor){
      this.sensor = sensor;
   }

   public void execute() throws SensorNotActivatedException{
      sensor.on();
   }
}
