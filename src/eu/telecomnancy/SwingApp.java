package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        SensorFactory factory = new SensorFactory();
        ISensor sensor = factory.getSensor("TemperatureSensor");
        new MainWindow(sensor);
    }

}
