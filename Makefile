

all : cli gui

cli :
	@javac -cp src/ -d dist/ src/eu/telecomnancy/App.java

gui :
	@javac -cp src/ -d dist/ src/eu/telecomnancy/SwingApp.java

.PHONY : run run-gui

run :
	@java -cp dist/ eu.telecomnancy.App

run-gui :
	@java -cp dist/ eu.telecomnancy.SwingApp
